'use strict';

function Router(routes) {
    try {
        if (!routes) {
            throw 'Error: Parametro obligatorio';
        }
        this.constructor(routes);
        this.init();
    } catch (e) {
        console.error(e);
    }
}

Router.prototype = {
    routes: undefined,
    rootElem: undefined,
    constructor: function (routes) {
        this.routes = routes;
        this.rootElem = document.getElementById('idRuteo');
    },
    init: function () {
        var r = this.routes;
        (function (scope, r) {
            window.addEventListener('hashchange', function (e) {
                scope.hasChanged(scope, r);
            });
        })(this, r);
        this.hasChanged(this, r);
    },
    hasChanged: function (scope, r) {
        if (window.location.hash.length > 0) {
            for (var i = 0, length = r.length; i < length; i++) {
                var route = r[i];
                if (route.isActiveRoute(window.location.hash.substr(1))) {
                    scope.goToRoute(route.htmlName);
                }
            }
        } else {
            for (var i = 0, length = r.length; i < length; i++) {
                var route = r[i];
                if (route.default) {
                    scope.goToRoute(route.htmlName);
                }
            }
        }
    },
    goToRoute: function (htmlName) {
        (function (scope) {
            var url = 'src/componentes/' + htmlName,
                xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    scope.rootElem.innerHTML = this.responseText;
                    businessLogic(url);
                }
            };

            xhttp.open('GET', url, true);
            // xhttp.overrideMimeType('application/javascript');
            xhttp.send();
        })(this);
    }
};

function businessLogic(url) {
    switch (url) {
        case 'src/componentes/admin.html':
            const arreglo = listaEmpleados["empleados"];
            var table = document.getElementById("tablaAdmin");

            for (var i = 0; i < arreglo.length; i++) {
                var row = table.insertRow();
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                var cell3 = row.insertCell(2);
                var cell4 = row.insertCell(3);
                var cell5 = row.insertCell(4);

                cell1.innerHTML = arreglo[i].codigo;
                cell2.innerHTML = arreglo[i].nombre;
                cell3.innerHTML = arreglo[i].apellido;
                cell4.innerHTML = arreglo[i].rol;
                cell5.innerHTML = "iconos";
            }

            break;
        case 'src/componentes/registroCliente.html':
            (() => {
                'use strict'
                const forms = document.querySelectorAll('.validarRegistroCliente')
                Array.from(forms).forEach(form => {
                    form.addEventListener('submit', event => {
                        if (!form.checkValidity()) {
                            event.preventDefault()
                            event.stopPropagation()
                        }
                        form.classList.add('was-validated')
                    }, false)
                })
            })()

            break;
        default:
            console.log('Javascript interno no utilizado');
    }
}