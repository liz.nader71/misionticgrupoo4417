/*==============================================================*/
/* Table: CATEGORIAS                                            */
/*==============================================================*/
create table CATEGORIAS
(
   ID_CATEGORIAS        int not null auto_increment  comment '',
   COD_CATEGORIAS       varchar(30) not null  comment '',
   NOMBRE_CATEGORIAS    varchar(100) not null  comment '',
   primary key (ID_CATEGORIAS)
);

/*==============================================================*/
/* Index: CONTROL_COD_CATEGORIAS                                */
/*==============================================================*/
create unique index CONTROL_COD_CATEGORIAS on CATEGORIAS
(
   COD_CATEGORIAS
);

/*==============================================================*/
/* Table: CLIENTES                                              */
/*==============================================================*/
create table CLIENTES
(
   ID_CLIENTES          int not null auto_increment  comment '',
   TIPO_DOCUMENTO_CLIENTES varchar(30) not null  comment '',
   NUMERO_IDENTIFICACION_CLIENTES varchar(30) not null  comment '',
   NOMBRE1_CLIENTES     varchar(30)  comment '',
   NOMBRE2_CLIENTES     varchar(30)  comment '',
   APELLIDO1_CLIENTES   varchar(30)  comment '',
   APELLIDO2_CLIENTES   varchar(30)  comment '',
   GENERO_CLIENTES      varchar(30)  comment '',
   primary key (ID_CLIENTES)
);

/*==============================================================*/
/* Index: CONTROL_NUMERO_IDENTIFICACION_CLIENTES                */
/*==============================================================*/
create unique index CONTROL_NUMERO_IDENTIFICACION_CLIENTES on CLIENTES
(
   TIPO_DOCUMENTO_CLIENTES,
   NUMERO_IDENTIFICACION_CLIENTES
);

/*==============================================================*/
/* Table: ITEMS                                                 */
/*==============================================================*/
create table ITEMS
(
   ID_ITEMS             int not null auto_increment  comment '',
   ID_VENTAS            int  comment '',
   ID_ITEMS_VENTAS      int not null  comment '',
   COD_ITEMS            numeric(20) not null  comment '',
   DESCRIPCION_ITEMS    varchar(100) not null  comment '',
   CANTIDAD_ITEMS       numeric(4,0) not null  comment '',
   PRECIO_TOTAL_ITEMS   numeric(10,2) not null  comment '',
   PRECIO_UNITARIO_ITEMS numeric(10,2) not null  comment '',
   primary key (ID_ITEMS)
);

/*==============================================================*/
/* Table: PRODUCTOS                                             */
/*==============================================================*/
create table PRODUCTOS
(
   ID_PRODUCTOS         int not null auto_increment  comment '',
   ID_CATEGORIAS        int  comment '',
   COD_PRODUCTOS        varchar(30) not null  comment '',
   NOMBRE_PRODUCTOS     varchar(200) not null  comment '',
   DESCRIPCION_PRODUCTOS varchar(100) not null  comment '',
   PRECIO_PRODUCTOS     numeric(10,2) not null  comment '',
   IMAGEN_PUBLICA_PRODUCTOS varchar(200)  comment '',
   IMAGEN_PRIVADA_PRODUCTOS varchar(200)  comment '',
   FICHA_TECNICA_PRODUCTOS varchar(300)  comment '',
   primary key (ID_PRODUCTOS)
);

/*==============================================================*/
/* Index: CONTROL_COD_PRODUCTOS                                 */
/*==============================================================*/
create unique index CONTROL_COD_PRODUCTOS on PRODUCTOS
(
   COD_PRODUCTOS
);

/*==============================================================*/
/* Table: VENTAS                                                */
/*==============================================================*/
create table VENTAS
(
   ID_VENTAS            int not null auto_increment  comment '',
   ID_CLIENTES          int  comment '',
   FECHA_VENTAS         datetime not null  comment '',
   TIPO_TRX_VENTAS      varchar(20) not null  comment '',
   MNT_PAGAR_VENTAS     numeric(10,2) not null  comment '',
   CANTIDAD_ITEM_VENTAS int not null  comment '',
   FECHA_TRX_INI_VENTAS datetime not null  comment '',
   FECHA_TRX_FIN_VENTAS datetime not null  comment '',
   primary key (ID_VENTAS)
);

alter table ITEMS add constraint FK_ITEMS_REFERENCE_VENTAS foreign key (ID_VENTAS)
      references VENTAS (ID_VENTAS) on delete restrict on update restrict;

alter table PRODUCTOS add constraint FK_PRODUCTO_REFERENCE_CATEGORI foreign key (ID_CATEGORIAS)
      references CATEGORIAS (ID_CATEGORIAS) on delete restrict on update restrict;

alter table VENTAS add constraint FK_VENTAS_REFERENCE_CLIENTES foreign key (ID_CLIENTES)
      references CLIENTES (ID_CLIENTES) on delete restrict on update restrict;

